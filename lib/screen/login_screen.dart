// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, avoid_print

import 'package:flutter/material.dart';
import 'package:form_and_authentication/helpers/database_helper.dart';
import 'package:form_and_authentication/models/user.dart';
import 'components/text_form_field_widget.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void onTapCreate() {
    Navigator.pushNamed(context, '/signup').then((value) => setState(() {
          emailController.clear();
          passwordController.clear();
        }));
  }

  void onTapSignIn() async {
    User? result;
    if (emailController.text != '' && passwordController.text != '') {
      result = await DatabaseHelper.instance
          .login(emailController.text, passwordController.text);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Semuanya wajib diisi!"),
      ));
    }
    if (result != null) {
      setState(() {
        emailController.clear();
        passwordController.clear();
      });
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Login berhasil"),
      ));
      Navigator.pushReplacementNamed(context, '/home', arguments: {
        'user': result,
      });
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Email atau password salah")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Hello",
                style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 16),
              Text("Sign in to your account"),
              SizedBox(height: 24),
              Form(
                child: Column(
                  children: [
                    TextFormFieldWidget(
                      textController: emailController,
                      label: "Email",
                      hint: "Input your email",
                      secure: false,
                    ),
                    TextFormFieldWidget(
                      textController: passwordController,
                      label: "Password",
                      hint: "Input your password",
                      secure: true,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        onTapSignIn();
                      },
                      child: Text("Sign In"),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Haven`t registered?"),
                  TextButton(
                    onPressed: () {
                      onTapCreate();
                    },
                    child: Text("Create"),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
