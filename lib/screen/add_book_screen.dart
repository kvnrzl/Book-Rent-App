// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:form_and_authentication/helpers/database_helper.dart';
import 'package:form_and_authentication/models/book.dart';
import 'package:form_and_authentication/screen/components/text_form_field_widget.dart';

class AddBookScreen extends StatefulWidget {
  const AddBookScreen({Key? key}) : super(key: key);

  @override
  State<AddBookScreen> createState() => _AddBookScreenState();
}

class _AddBookScreenState extends State<AddBookScreen> {
  TextEditingController judulController = TextEditingController();
  TextEditingController deskripsiController = TextEditingController();
  TextEditingController genreController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController gambarController = TextEditingController();
  String? isPublish;

  @override
  void dispose() {
    judulController.dispose();
    deskripsiController.dispose();
    genreController.dispose();
    hargaController.dispose();
    gambarController.dispose();
    super.dispose();
  }

  void onTapAddOrUpdate(int uid, {Book? book}) async {
    int? result;
    if (judulController.text == '' ||
        deskripsiController.text == '' ||
        genreController.text == '' ||
        hargaController.text == '' ||
        gambarController.text == '' ||
        isPublish == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Semuanya wajib diisi!"),
      ));
    } else {
      if (book != null) {
        result = await DatabaseHelper.instance.updateBook(
          Book(
              id: book.id,
              judul: judulController.text,
              deskripsi: deskripsiController.text,
              genre: genreController.text,
              harga: hargaController.text,
              gambar: gambarController.text,
              pemilikId: uid,
              isPublish: isPublish),
        );
      } else {
        result = await DatabaseHelper.instance.addBook(
          Book(
              judul: judulController.text,
              deskripsi: deskripsiController.text,
              genre: genreController.text,
              harga: hargaController.text,
              gambar: gambarController.text,
              pemilikId: uid,
              isPublish: isPublish),
        );
      }
    }
    if (result.runtimeType == int) {
      setState(() {
        judulController.clear();
        deskripsiController.clear();
        genreController.clear();
        hargaController.clear();
        gambarController.clear();
      });
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Berhasil"),
      ));
      Navigator.pop(context);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Gagal"),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final argument = ModalRoute.of(context)!.settings.arguments as Map;
    var uid = argument['uid'];
    Book? book = argument['book'];

    if (book != null) {
      judulController.text = book.judul!;
      deskripsiController.text = book.deskripsi!;
      genreController.text = book.genre!;
      hargaController.text = book.harga!;
      gambarController.text = book.gambar!;
      isPublish = book.isPublish!;
    }

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Penambahan Buku"),
                TextFormFieldWidget(
                    textController: judulController,
                    label: "Judul buku",
                    hint: "Masukkan judul buku",
                    secure: false),
                TextFormFieldWidget(
                    textController: deskripsiController,
                    label: "Deskripsi",
                    hint: "Masukkan deskripsi",
                    secure: false),
                TextFormFieldWidget(
                    textController: genreController,
                    label: "Genre",
                    hint: "Masukkan genre",
                    secure: false),
                TextFormFieldWidget(
                    textController: hargaController,
                    label: "Harga",
                    hint: "Masukkan harga",
                    secure: false),
                TextFormFieldWidget(
                    textController: gambarController,
                    label: "Gambar",
                    hint: "Masukkan gambar source dari internet",
                    secure: false),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 8),
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: DropdownButton(
                    underline: Divider(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(16),
                    isExpanded: true,
                    items: <DropdownMenuItem<String>>[
                      DropdownMenuItem(
                        child: Text("Published"),
                        value: "Published",
                      ),
                      DropdownMenuItem(
                        child: Text("Not Published"),
                        value: "Not Published",
                      ),
                    ],
                    hint: Text("Status Publish"),
                    value: isPublish,
                    onChanged: (String? value) {
                      setState(() {
                        isPublish = value;
                      });
                    },
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    onTapAddOrUpdate(uid, book: book);
                  },
                  child: Text(book != null ? "Update" : "Tambahkan"),
                ),
                TextButton(
                  onPressed: () async {
                    Navigator.pop(context);
                  },
                  child: Text("Back"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
