// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_print

import 'package:flutter/material.dart';
import 'package:form_and_authentication/helpers/database_helper.dart';
import 'package:form_and_authentication/models/user.dart';
import 'package:form_and_authentication/screen/components/text_form_field_widget.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  String? roles;

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    phoneController.dispose();
    addressController.dispose();
    super.dispose();
  }

  void onTapCreate() async {
    bool? statusSignup;
    if (nameController.text != '' &&
        emailController.text != '' &&
        passwordController.text != '' &&
        phoneController.text != '' &&
        addressController.text != '' &&
        roles != null) {
      statusSignup = await DatabaseHelper.instance.create(
        User(
            name: nameController.text,
            email: emailController.text,
            password: passwordController.text,
            phone: phoneController.text,
            address: addressController.text,
            roles: roles),
      );
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Semuanya wajib diisi!")));
    }

    if (statusSignup == true) {
      setState(() {
        nameController.clear();
        emailController.clear();
        passwordController.clear();
        phoneController.clear();
        addressController.clear();
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Berhasil! Silahkan Login!")));
      Navigator.pushReplacementNamed(context, '/');
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Email telah digunakan")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Create your account"),
                TextFormFieldWidget(
                    textController: nameController,
                    label: "Name",
                    hint: "Input your name",
                    secure: false),
                TextFormFieldWidget(
                    textController: emailController,
                    label: "Email",
                    hint: "Input your email",
                    secure: false),
                TextFormFieldWidget(
                    textController: passwordController,
                    label: "Password",
                    hint: "Input your password",
                    secure: true),
                TextFormFieldWidget(
                    textController: phoneController,
                    label: "Phone number",
                    hint: "Input your phone number",
                    secure: false),
                TextFormFieldWidget(
                    textController: addressController,
                    label: "Address",
                    hint: "Input your address",
                    secure: false),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 8),
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: DropdownButton(
                    underline: Divider(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(16),
                    isExpanded: true,
                    items: <DropdownMenuItem<String>>[
                      DropdownMenuItem(
                        child: Text("Peminjam"),
                        value: "Peminjam",
                      ),
                      DropdownMenuItem(
                        child: Text("Pemilik"),
                        value: "Pemilik",
                      ),
                    ],
                    hint: Text("Roles"),
                    value: roles,
                    onChanged: (String? value) {
                      setState(() {
                        roles = value;
                      });
                    },
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    onTapCreate();
                  },
                  child: Text("Create"),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Have registered?"),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Sign In"),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
