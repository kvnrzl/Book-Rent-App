// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:form_and_authentication/helpers/database_helper.dart';
import 'package:form_and_authentication/models/book.dart';
import 'package:form_and_authentication/models/user.dart';
import 'package:form_and_authentication/screen/components/list_book_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<List<Book>>? resultFuture;

  @override
  void initState() {
    super.initState();
    resultFuture = DatabaseHelper.instance.getBookForBorrower();
  }

  void showDialogSewaBuku(BuildContext context, int bookId, int uid) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed: () {
        onTapSewaBuku(bookId, uid);
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Sewa Buku"),
      content: Text("Apakah anda yakin untuk menyewa buku tersebut?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void onTapSewaBuku(int bookId, int uid) async {
    await DatabaseHelper.instance.updateBookBorrower(
      Book(
        id: bookId,
        status: "Dipinjam",
        peminjamId: uid,
      ),
    );
    setState(() {
      resultFuture = DatabaseHelper.instance.getBookForBorrower();
    });
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Buku berhasil dipinjam"),
    ));
  }

  void showDialogDeskripsi(BuildContext context, {required String deskripsi}) {
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Deskripsi"),
      content: Text(deskripsi),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final argument = ModalRoute.of(context)!.settings.arguments as Map;
    User user = argument['user'];

    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: UserAccountsDrawerHeader(
                accountEmail: Text(""),
                accountName: Row(
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      margin: EdgeInsets.only(right: 16),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: CircleAvatar(
                        foregroundImage: AssetImage("assets/images/Takiya.jpg"),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(user.email),
                        Text(user.name),
                      ],
                    )
                  ],
                ),
              ),
            ),
            ListTile(
              title: const Text('Home'),
              leading: Icon(Icons.home),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            user.roles == "Pemilik"
                ? ListTile(
                    leading: Icon(Icons.menu_book_outlined),
                    title: const Text('Buku Saya'),
                    onTap: () {
                      Navigator.pushNamed(context, '/mybook', arguments: {
                        'uid': user.id
                      }).then((_) => setState(() {
                            resultFuture =
                                DatabaseHelper.instance.getBookForBorrower();
                          }));
                    })
                : ListTile(
                    leading: Icon(Icons.restore_page_rounded),
                    title: const Text('Pinjaman Saya'),
                    onTap: () {
                      Navigator.pushNamed(
                          context, '/borrowingbook', arguments: {
                        'user': user
                      }).then((_) => setState(() {
                            resultFuture =
                                DatabaseHelper.instance.getBookForBorrower();
                          }));
                    },
                  ),
            ListTile(
              leading: Icon(Icons.exit_to_app_rounded),
              title: const Text('Log Out'),
              onTap: () {
                Navigator.pushReplacementNamed(context, '/');
              },
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: user.roles == 'Pemilik'
            ? Center(
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                    border: Border.all(),
                  ),
                  child: Text(
                    "Anda sebagai pemilik yang akan menyewakan buku\nSilahkan menuju ke halaman 'Buku Saya' untuk menambahkan daftar buku yang akan disewakan",
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            : FutureBuilder<List<Book>>(
                future: resultFuture,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(child: Text("Loading..."));
                  }
                  return snapshot.data!.isEmpty
                      ? Center(
                          child:
                              Text("Tidak ada buku yang dapat untuk dipinjam"))
                      : ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            var book = snapshot.data![index];

                            return ListBookWidget(
                              book: book,
                              box1: book.judul!,
                              box2: book.harga!,
                              nameButtonLeft: "Deskripsi",
                              onPressButtonLeft: () {
                                showDialogDeskripsi(context,
                                    deskripsi: book.deskripsi!);
                              },
                              nameButtonRight: "Sewa Buku",
                              onPressButtonRight: () {
                                showDialogSewaBuku(context, book.id!, user.id!);
                              },
                            );
                          },
                        );
                },
              ),
      ),
    );
  }
}
