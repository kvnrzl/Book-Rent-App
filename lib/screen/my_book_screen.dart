// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:form_and_authentication/helpers/database_helper.dart';
import 'package:form_and_authentication/models/book.dart';
import 'package:form_and_authentication/screen/components/list_book_widget.dart';

class MyBookScreen extends StatefulWidget {
  const MyBookScreen({Key? key}) : super(key: key);

  @override
  State<MyBookScreen> createState() => _MyBookScreenState();
}

class _MyBookScreenState extends State<MyBookScreen> {
  void showDialogDelete(BuildContext context, int bookId, int uid) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed: () {
        onTapDelete(bookId, uid);
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Delete"),
      content:
          Text("Apakah anda yakin untuk menghapus buku tersebut dari daftar?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void onTapEdit(Book book, int uid) {
    Navigator.pushNamed(context, '/addbook',
        arguments: {'book': book, 'uid': uid}).then(
      (value) => setState(() {
        DatabaseHelper.instance.getBook(uid, borrower: false);
      }),
    );
  }

  void onTapDelete(int bookId, int uid) async {
    int result = await DatabaseHelper.instance.deleteBook(bookId);
    if (result.runtimeType == int) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Berhasil menghapus")));
      setState(() {
        DatabaseHelper.instance.getBook(uid, borrower: false);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final argument = ModalRoute.of(context)!.settings.arguments as Map;
    var uid = argument['uid'];

    return Scaffold(
      appBar: AppBar(
        title: Text("Buku Saya"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/addbook', arguments: {'uid': uid})
              .then((value) => setState(() {
                    DatabaseHelper.instance.getBook(uid, borrower: false);
                  }));
        },
        child: Icon(Icons.add),
      ),
      body: SafeArea(
        child: FutureBuilder<List<Book>>(
          future: DatabaseHelper.instance.getBook(uid, borrower: false),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: Text("Loading..."));
            }
            return snapshot.data!.isEmpty
                ? Center(child: Text("Tambahkan buku untuk anda pinjamkan"))
                : ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      var book = snapshot.data![index];

                      return ListBookWidget(
                        book: book,
                        box1: book.judul!,
                        box2: book.isPublish!,
                        nameButtonLeft: "Edit",
                        onPressButtonLeft: () {
                          onTapEdit(book, uid);
                        },
                        nameButtonRight: "Hapus",
                        onPressButtonRight: () {
                          showDialogDelete(context, book.id!, uid);
                        },
                      );
                    });
            // ListView(
            //     children: snapshot.data!
            //         .map((book) => ListBookWidget(
            //               book: book,
            //               uid: uid,
            //             ))
            //         .toList(),
            //   );
          },
        ),
      ),
    );
  }
}
