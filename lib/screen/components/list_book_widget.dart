// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:form_and_authentication/models/book.dart';

class ListBookWidget extends StatelessWidget {
  const ListBookWidget({
    Key? key,
    required this.book,
    required this.onPressButtonLeft,
    required this.onPressButtonRight,
    required this.nameButtonLeft,
    required this.nameButtonRight,
    required this.box1,
    required this.box2,
  }) : super(key: key);

  final Book? book;
  final VoidCallback onPressButtonLeft;
  final VoidCallback onPressButtonRight;
  final String nameButtonLeft;
  final String nameButtonRight;
  final String box1, box2;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.all(8),
              height: 115,
              decoration: BoxDecoration(
                color: Colors.black,
                image: DecorationImage(
                  image: NetworkImage(book!.gambar!),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  margin: EdgeInsets.all(4),
                  height: 30,
                  decoration: BoxDecoration(
                    border: Border.all(),
                  ),
                  child: Text(
                    box1,
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(4),
                  height: 30,
                  decoration: BoxDecoration(
                    border: Border.all(),
                  ),
                  child: Text(
                    box2,
                    textAlign: TextAlign.center,
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: TextButton(
                        onPressed: onPressButtonLeft,
                        child: Text(nameButtonLeft),
                      ),
                    ),
                    Expanded(
                      child: TextButton(
                        onPressed: onPressButtonRight,
                        child: Text(nameButtonRight),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
