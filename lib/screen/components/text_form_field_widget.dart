// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class TextFormFieldWidget extends StatelessWidget {
  const TextFormFieldWidget({
    Key? key,
    required this.textController,
    required this.label,
    required this.hint,
    required this.secure,
  }) : super(key: key);

  final TextEditingController textController;
  final String label;
  final String hint;
  final bool secure;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        border: Border.all(),
        borderRadius: BorderRadius.circular(16),
      ),
      child: TextFormField(
        controller: textController,
        obscureText: secure,
        validator: (val) {
          if (val == null || val.isEmpty) {
            return "Wajib diisi";
          }
          return null;
        },
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          labelText: label,
          hintText: hint,
          border: UnderlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }
}
