// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:form_and_authentication/helpers/database_helper.dart';
import 'package:form_and_authentication/models/book.dart';
import 'package:form_and_authentication/models/user.dart';
import 'package:form_and_authentication/screen/components/list_book_widget.dart';

class MyBorrowingScreen extends StatefulWidget {
  const MyBorrowingScreen({Key? key}) : super(key: key);

  @override
  State<MyBorrowingScreen> createState() => _MyBorrowingScreenState();
}

class _MyBorrowingScreenState extends State<MyBorrowingScreen> {
  void showDialogKembalikan(BuildContext context, int bookId) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed: () {
        onTapKembalikan(bookId);
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Kembalikan Buku"),
      content: Text("Apakah anda ingin mengembalikan buku tersebut?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void onTapKembalikan(int bookId) async {
    await DatabaseHelper.instance.updateBookBorrower(
      Book(
        id: bookId,
        status: "Tidak Dipinjam",
        peminjamId: -1,
      ),
    );
    setState(() {
      DatabaseHelper.instance.getBookForBorrower();
    });
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Buku berhasil dikembalikan"),
    ));
  }

  void showDialogDeskripsi(BuildContext context, {required String deskripsi}) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Deskripsi"),
      content: Text(deskripsi),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final argument = ModalRoute.of(context)!.settings.arguments as Map;
    User user = argument['user'];

    return Scaffold(
      appBar: AppBar(
        title: Text("Daftar Pinjaman"),
      ),
      body: SafeArea(
        child: FutureBuilder<List<Book>>(
          future: DatabaseHelper.instance.getBook(user.id!, borrower: true),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: Text("Loading..."));
            }
            return snapshot.data!.isEmpty
                ? Center(child: Text("Tidak ada buku yang dipinjam"))
                : ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      var book = snapshot.data![index];

                      return ListBookWidget(
                        book: book,
                        box1: book.judul!,
                        box2: book.harga!,
                        nameButtonLeft: "Deskripsi",
                        onPressButtonLeft: () {
                          showDialogDeskripsi(context,
                              deskripsi: book.deskripsi!);
                        },
                        nameButtonRight: "Kembalikan",
                        onPressButtonRight: () {
                          showDialogKembalikan(context, book.id!);
                        },
                      );
                    },
                  );
          },
        ),
      ),
    );
  }
}
