class Book {
  final int? id;
  final String? judul;
  final String? deskripsi;
  final String? genre;
  final String? harga;
  final String? gambar;
  final int? pemilikId;
  final int? peminjamId;
  final String? status;
  final String? isPublish;
  // ID, genre_id, judul_buku, deskripsi, harga, gambar, pemilik_id, peminjam_id, status, is_publish

  Book(
      {this.id,
      this.judul,
      this.deskripsi,
      this.genre,
      this.harga,
      this.gambar,
      this.pemilikId = -1,
      this.peminjamId = -1,
      this.status = "Tidak Dipinjam",
      this.isPublish});

  factory Book.fromMap(Map<String, dynamic> json) => Book(
      id: json['id'],
      judul: json['judul'],
      deskripsi: json['deskripsi'],
      genre: json['genre'],
      harga: json['harga'],
      gambar: json['gambar'],
      pemilikId: json['pemilikId'],
      peminjamId: json['peminjamId'],
      status: json['status'],
      isPublish: json['isPublish']);

  Map<String, dynamic> toMap() => {
        'id': id,
        'genre': genre,
        'judul': judul,
        'deskripsi': deskripsi,
        'harga': harga,
        'gambar': gambar,
        'pemilikId': pemilikId,
        'peminjamId': peminjamId,
        'status': status,
        'isPublish': isPublish,
      };
}
