class User {
  final int? id;
  final String name;
  final String email;
  final String password;
  final String phone;
  final String address;
  final String? roles;

  User(
      {this.id,
      required this.name,
      required this.email,
      required this.password,
      required this.phone,
      required this.address,
      required this.roles});

  factory User.fromMap(Map<String, dynamic> json) => User(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        password: json['password'],
        phone: json['phone'],
        address: json['address'],
        roles: json['roles'],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'email': email,
        'password': password,
        'phone': phone,
        'address': address,
        'roles': roles,
      };
}
