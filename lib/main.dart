// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:form_and_authentication/screen/add_book_screen.dart';
import 'package:form_and_authentication/screen/home_screen.dart';
import 'package:form_and_authentication/screen/my_book_screen.dart';
import 'package:form_and_authentication/screen/my_borrowing_screen.dart';
import 'package:form_and_authentication/screen/signup_screen.dart';
import 'screen/login_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lembang Bandung',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // brightness: Brightness.dark,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => LoginScreen(),
        '/signup': (context) => SignupScreen(),
        '/home': (context) => HomeScreen(),
        '/mybook': (context) => MyBookScreen(),
        '/addbook': (context) => AddBookScreen(),
        '/borrowingbook': (context) => MyBorrowingScreen(),
      },
      // home: LoginScreen(),
    );
  }
}
