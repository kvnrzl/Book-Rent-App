// ignore_for_file: avoid_print

import 'dart:io';

import 'package:form_and_authentication/models/book.dart';
import 'package:form_and_authentication/models/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'newtest.db');
    print("Posisi database :" + path);
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE IF NOT EXISTS users(
        id INTEGER PRIMARY KEY,
        name TEXT,
        email TEXT,
        password TEXT,
        phone TEXT,
        address TEXT,
        roles TEXT
      )
      ''');

    await db.execute('''
      CREATE TABLE IF NOT EXISTS books(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        judul TEXT,
        deskripsi TEXT,
        genre TEXT,
        harga TEXT,
        gambar TEXT,
        pemilikId INTEGER,
        peminjamId INTEGER,
        status TEXT,
        isPublish TEXT,
        FOREIGN KEY(pemilikId) REFERENCES users(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
        FOREIGN KEY(peminjamId) REFERENCES users(id) ON DELETE NO ACTION ON UPDATE NO ACTION
      )
    ''');
  }

  Future<List<Book>> getBookForBorrower() async {
    Database db = await instance.database;
    String query =
        "SELECT * FROM books WHERE isPublish = 'Published' AND status = 'Tidak Dipinjam'";
    var result = await db.rawQuery(query);
    return result.isNotEmpty ? result.map((e) => Book.fromMap(e)).toList() : [];
  }

  Future<void> updateBookBorrower(Book book) async {
    Database db = await instance.database;
    String query =
        "UPDATE books SET status = '${book.status}', peminjamId = '${book.peminjamId}' WHERE id = '${book.id}'";
    await db.rawQuery(query);
  }

  // ini buat nampilin daftar buku yang dimili
  Future<List<Book>> getBook(int id, {required bool borrower}) async {
    Database db = await instance.database;
    String query = borrower
        ? "SELECT * FROM books WHERE peminjamId = '$id'"
        : "SELECT * FROM books WHERE pemilikId = '$id'";
    var result = await db.rawQuery(query);
    return result.isNotEmpty ? result.map((e) => Book.fromMap(e)).toList() : [];
  }

  Future<int> addBook(Book book) async {
    Database db = await instance.database;
    return await db.insert('books', book.toMap());
  }

  Future<int> deleteBook(int id) async {
    Database db = await instance.database;
    return await db.delete('books', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> updateBook(Book book) async {
    Database db = await instance.database;
    return await db
        .update('books', book.toMap(), where: "id = ?", whereArgs: [book.id]);
  }

  Future<User?> login(String email, String password) async {
    Database db = await instance.database;
    String query =
        "SELECT * FROM users WHERE email = '$email' AND password = '$password'";
    var checkLoginQuery = await db.rawQuery(query);

    // checkLoginQuery.isNotEmpty
    //     ? checkLoginQuery.map((e) => User.fromMap(e)).toList()
    //     : [];

    return checkLoginQuery.isNotEmpty ? User.fromMap(checkLoginQuery[0]) : null;
    // return checkLoginQuery.isEmpty ? false : true;
  }

  Future<bool> create(User user) async {
    Database db = await instance.database;
    String query = "SELECT * FROM users WHERE email = '${user.email}'";
    var signupCheckQuery = await db.rawQuery(query);
    if (signupCheckQuery.isEmpty) {
      await db.insert('users', user.toMap());
      return true;
    } else {
      return false;
    }
  }

  // ini buat ngetest aja
  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('users', where: 'id = ?', whereArgs: [id]);
  }

  // ini buat ngetest aja
  Future<int> update(User user) async {
    Database db = await instance.database;
    return await db
        .update('users', user.toMap(), where: "id = ?", whereArgs: [user.id]);
  }
}
